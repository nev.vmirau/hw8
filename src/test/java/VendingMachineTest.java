import org.junit.Test;
import root.vending.VendingMachine;

import static org.junit.jupiter.api.Assertions.*;

public class VendingMachineTest {

    private static final long MACHINE_KEY = 117345294655382L;

    @Test
    public void newVendingMachineMustHaveZeroProducts() {
        var machine = new VendingMachine();
        assertEquals(machine.getNumberOfProduct1(), 0);
        assertEquals(machine.getNumberOfProduct2(), 0);
        assertEquals(machine.getCurrentSum(), 0);
    }

    @Test
    public void newVendingMachineMustHaveZeroCoins() {
        var machine = new VendingMachine();
        assertEquals(machine.getCoins1(), 0);
        assertEquals(machine.getCoins2(), 0);
        assertEquals(machine.getCurrentBalance(), 0);
    }

    @Test
    public void enterAdminModeWithInsertedMoneyMustReturnCannotPerformResponse() {
        var machine = new VendingMachine();
        machine.putCoin1();
        assertTrue(machine.getCurrentBalance() > 0);
        var response = machine.enterAdminMode(MACHINE_KEY);
        assertEquals(response, VendingMachine.Response.CANNOT_PERFORM);
        assertEquals(machine.getCurrentMode(), VendingMachine.Mode.OPERATION);
    }

    @Test
    public void enterAdminModelWithRightKeyMustReturnOkResponse() {
        var machine = new VendingMachine();
        var response = machine.enterAdminMode(MACHINE_KEY);
        assertEquals(response, VendingMachine.Response.OK);
        assertEquals(machine.getCurrentMode(), VendingMachine.Mode.ADMINISTERING);
    }

    @Test
    public void enterAdminModeWithWrongKeyMustReturnInvalidParamResponse() {
        var wrongKey = 1;
        var machine = new VendingMachine();
        var response = machine.enterAdminMode(wrongKey);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        assertEquals(machine.getCurrentMode(), VendingMachine.Mode.OPERATION);
    }

    @Test
    public void fillProductsNotInAdminModeMustReturnIllegalOperationResponse() {
        var machine = new VendingMachine();
        var response = machine.fillProducts();
        assertEquals(response, VendingMachine.Response.ILLEGAL_OPERATION);
    }

    @Test
    public void fillProductsInAdminModeMustReturnOkResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.fillProducts();
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void fillProductsInAdminModeMustAddProducts() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var num_products1_before = machine.getNumberOfProduct1();
        var num_products2_before = machine.getNumberOfProduct2();
        machine.fillProducts();
        var num_products1 = machine.getNumberOfProduct1();
        var num_products2 = machine.getNumberOfProduct2();
        assertNotEquals(machine.getNumberOfProduct1(), num_products1_before);
        assertNotEquals(machine.getNumberOfProduct2(), num_products2_before);
        assertTrue(num_products1 > num_products1_before);
        assertTrue(num_products2 > num_products2_before);
    }

    @Test
    public void fillCoinsNotInAdminModeReturnsIllegalOperationResponse() {
        var machine = new VendingMachine();
        var response = machine.fillCoins(1, 1);
        assertEquals(response, VendingMachine.Response.ILLEGAL_OPERATION);
    }

    @Test
    public void fillCoinsInAdminModeReturnsOkResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.fillCoins(1, 1);
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void fillCoinsWithNegativeOrZeroParamsMustReturnInvalidParamResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.fillCoins(-1, 1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.fillCoins(1, -1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.fillCoins(0, 1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.fillCoins(1, 0);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
    }

    @Test
    public void fillCoinsWithTooBigParamsMustReturnInvalidParamResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.fillCoins(Integer.MAX_VALUE, 1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.fillCoins(1, Integer.MAX_VALUE);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
    }

    @Test
    public void fillCoinsWithValidParamsMustReturnOkResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.fillCoins(1, 1);
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void getCoins1InOperationModeMustReturnZero() {
        var machine = new VendingMachine();
        assertEquals(0, machine.getCoins1());
    }

    @Test
    public void getCoins1InAdminModeMustReturnCoins1() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillCoins(1, 1);
        assertEquals(1, machine.getCoins1());
    }

    @Test
    public void getCoins2InOperationModeMustReturnZero() {
        var machine = new VendingMachine();
        assertEquals(0, machine.getCoins2());
    }

    @Test
    public void getCoins2InAdminModeMustReturnCoins2() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillCoins(1, 1);
        assertEquals(1, machine.getCoins2());
    }

    @Test
    public void exitAdminModeSetsMachineModeToOperation() {
        var machine = new VendingMachine();
        machine.exitAdminMode();
        assertEquals(machine.getCurrentMode(), VendingMachine.Mode.OPERATION);
        machine.enterAdminMode(MACHINE_KEY);
        machine.exitAdminMode();
        assertEquals(machine.getCurrentMode(), VendingMachine.Mode.OPERATION);
    }

    @Test
    public void setPricesNotInAdminModeMustReturnIllegalOperationResponse() {
        var machine = new VendingMachine();
        var response = machine.setPrices(1, 1);
        assertEquals(response, VendingMachine.Response.ILLEGAL_OPERATION);
    }

    @Test
    public void setPricesWithOkParamsMustReturnOkResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.setPrices(1, 1);
        assertEquals(response, VendingMachine.Response.OK);
        assertEquals(machine.getPrice1(), 1);
        assertEquals(machine.getPrice2(), 1);
    }

    @Test
    public void setPricesWithNegativeOrZeroParamsMustReturnInvalidParamResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.setPrices(-1, 1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.setPrices(1, -1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.setPrices(0, 1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.setPrices(1, 0);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
    }

    @Test
    public void putCoin1InAdminModeMustReturnIllegalOperationResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.putCoin1();
        assertEquals(response, VendingMachine.Response.ILLEGAL_OPERATION);
    }

    @Test
    public void putCoin1NotInAdminModeMustReturnOkResponse() {
        var machine = new VendingMachine();
        var response = machine.putCoin1();
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void putCoin1WhenNumCoins1IsMaxMustReturnCannotPerformResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillCoins(50, 1);
        machine.exitAdminMode();
        var response = machine.putCoin1();
        assertEquals(response, VendingMachine.Response.CANNOT_PERFORM);
    }

    @Test
    public void putCoin2InAdminModeMustReturnIllegalOperationResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.putCoin2();
        assertEquals(response, VendingMachine.Response.ILLEGAL_OPERATION);
    }

    @Test
    public void putCoin2NotInAdminModeMustReturnOkResponse() {
        var machine = new VendingMachine();
        var response = machine.putCoin2();
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void putCoin2WhenNumCoins2IsMaxMustReturnCannotPerformResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillCoins(1, 50);
        machine.exitAdminMode();
        var response = machine.putCoin2();
        assertEquals(response, VendingMachine.Response.CANNOT_PERFORM);
    }

    @Test
    public void returnMoneyInAdminModeMustReturnIllegalOperationResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.returnMoney();
        assertEquals(response, VendingMachine.Response.ILLEGAL_OPERATION);
    }

    @Test
    public void returnMoneyWithZeroBalanceMustReturnOkResponse() {
        var machine = new VendingMachine();
        var balance = machine.getCurrentBalance();
        assertEquals(0, balance);
        var response = machine.returnMoney();
        assertEquals(VendingMachine.Response.OK, response);
    }

    //@Test
    //public void returnMoneyWithInvalidNumberOfCoinsMustReturnTooBigChangeResponse() {
    //    var machine = new VendingMachine();
    //    machine.enterAdminMode(MACHINE_KEY);

//    }

    @Test
    public void returnMoneyWithBalanceMoreThanNumCoins2MustAlsoGiveCoins1AndReturnOkResponse() {
        var machine = new VendingMachine();
        machine.putCoin1();
        machine.putCoin1();
        machine.putCoin1();
        machine.putCoin2();
        var response = machine.returnMoney();
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void returnMoneyWithEvenBalanceMustReturnOkResponse() {
        var machine = new VendingMachine();
        machine.putCoin2();
        machine.putCoin2();
        machine.putCoin2();
        machine.putCoin2();
        var response = machine.returnMoney();
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void giveProduct1InAdminModeMustReturnIllegalOperation() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.giveProduct1(1);
        assertEquals(response, VendingMachine.Response.ILLEGAL_OPERATION);
    }

    @Test
    public void giveProduct1WithNegativeOrZeroOrMoreThanMaxParamMustReturnInvalidParam() {
        var machine = new VendingMachine();
        var response = machine.giveProduct1(-1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.giveProduct1(0);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.giveProduct1(Integer.MAX_VALUE);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
    }

    @Test
    public void giveProduct1WithParamMoreThanExistingNumMustReturnInsufficientProductResponse() {
        var machine = new VendingMachine();
        assertEquals(VendingMachine.Response.INSUFFICIENT_PRODUCT, machine.giveProduct1(1));
    }

    @Test
    public void giveProduct1WithTooLessInsertedMoneyMustReturnInsufficientMoneyResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillProducts();
        machine.exitAdminMode();
        machine.putCoin1();
        var response = machine.giveProduct1(1);
        assertEquals(response, VendingMachine.Response.INSUFFICIENT_MONEY);
    }

    @Test
    public void giveProduct1WithOddChangeWithNoCoins1MustReturnUnsuitableChangeResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.setPrices(5, 1);
        machine.fillProducts();
        machine.exitAdminMode();
        machine.putCoin2();
        machine.putCoin2();
        machine.putCoin2();
        var response = machine.giveProduct1(1);
        assertEquals(response, VendingMachine.Response.UNSUITABLE_CHANGE);
    }

    @Test
    public void giveProduct1WithEvenChangeMustReturnOkResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillProducts();
        machine.exitAdminMode();
        machine.putCoin2();
        machine.putCoin2();
        machine.putCoin2();
        machine.putCoin2();
        var response = machine.giveProduct1(1);
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void giveProduct1WithChangeMoreThanCoins2ButHasCoins1MustReturnOkResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillProducts();
        machine.setPrices(2, 1);
        machine.exitAdminMode();
        machine.putCoin1();
        machine.putCoin1();
        machine.putCoin1();
        machine.putCoin1();
        var response = machine.giveProduct1(1);
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void giveProduct2InAdminModeMustReturnIllegalOperation() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        var response = machine.giveProduct2(1);
        assertEquals(response, VendingMachine.Response.ILLEGAL_OPERATION);
    }

    @Test
    public void giveProduct2WithNegativeOrZeroOrMoreThanMaxParamMustReturnInvalidParam() {
        var machine = new VendingMachine();
        var response = machine.giveProduct2(-1);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.giveProduct2(0);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
        response = machine.giveProduct2(Integer.MAX_VALUE);
        assertEquals(response, VendingMachine.Response.INVALID_PARAM);
    }

    @Test
    public void giveProduct2WithParamMoreThanExistingNumMustReturnInsufficientProductResponse() {
        var machine = new VendingMachine();
        var response = machine.giveProduct1(2);
        assertEquals(response, VendingMachine.Response.INSUFFICIENT_PRODUCT);
    }

    @Test
    public void giveProduct2WithTooLessInsertedMoneyMustReturnInsufficientMoneyResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillProducts();
        machine.exitAdminMode();
        machine.putCoin1();
        var response = machine.giveProduct2(1);
        assertEquals(response, VendingMachine.Response.INSUFFICIENT_MONEY);
    }

    @Test
    public void giveProduct2WithOddChangeWithNoCoins1MustReturnUnsuitableChangeResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.setPrices(1, 5);
        machine.fillProducts();
        machine.exitAdminMode();
        machine.putCoin2();
        machine.putCoin2();
        machine.putCoin2();
        var response = machine.giveProduct2(1);
        assertEquals(response, VendingMachine.Response.UNSUITABLE_CHANGE);
    }

    @Test
    public void giveProduct2WithEvenChangeMustReturnOkResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillProducts();
        machine.setPrices(1, 6);
        machine.exitAdminMode();
        machine.putCoin2();
        machine.putCoin2();
        machine.putCoin2();
        machine.putCoin2();
        var response = machine.giveProduct2(1);
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void giveProduct2WithChangeMoreThanCoins2ButHasCoins1MustReturnOkResponse() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillProducts();
        machine.setPrices(1, 2);
        machine.exitAdminMode();
        machine.putCoin1();
        machine.putCoin1();
        machine.putCoin1();
        machine.putCoin1();
        var response = machine.giveProduct2(1);
        assertEquals(response, VendingMachine.Response.OK);
    }

    @Test
    public void getCurrentSumInOperationModeMustReturnZero() {
        var machine = new VendingMachine();
        var response = machine.getCurrentSum();
        assertEquals(0, response);
    }

    @Test
    public void getCurrentSumInAdminModeMustReturnRealSum() {
        var machine = new VendingMachine();
        machine.enterAdminMode(MACHINE_KEY);
        machine.fillCoins(1, 1);
        assertEquals(3, machine.getCurrentSum());
    }

}
